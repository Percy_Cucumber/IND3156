# Project Overview
My original plan for this project was to create an original piece of software in Python, most likely in the form of some kind of mini-game.
However, this proved unfeasible due to the time constraint, as well as issues with my computer.
I opted instead to re-create a version of the old-school “snake” arcade game in Python, as there was a great tutorial by the name of “wormy,” that I could turn to when I encountered any serious problems and/or got stuck (https://inventwithpython.com/pygame/).
Once I had my code finished, I made sure to create a detailed Jupyter notebook, so that I have notes available for when I inevitably return to this kind of project in the future.
During the process of installing plugins for my python (specifically pygame), I screwed up something in my terminal's path.
After extensive research and troubleshooting, I was able to find a "solution" of sorts, though it was only able to counter the effects of my broken path in a specific window, and every new window opened stayed equally broken.
Unable to fix this any farther, I focused my efforts towards something else I have been meaning to try in this course… exploring and learning yet another programming language.
I chose Golang, as I believe it is very powerful, and will become highly used in the near future.
I’ve done a fair amount of research on it after Ian informed me of it’s existence, and it seems like a very efficient language.
This makes sense as Google created it with the goal of simplifying their code, by replacing all of their existing C, C++, and Java code with new Golang code.
I continued to spend my long weekend learning about Golang, making sure to take extensive notes along the way via Jupyter.
After reconvening in class on Tuesday, Ian was able to help me sort out some of the issues on my computer, giving me the option of returning to my original concept of building a game.
Unable to back away from the challange, I went back and fixed up my original `wormy.py` code.
Feeling a bit more confident, I found example code for the old arcade game "space invaders", and proceded to modify it in order to create a unique minigame by the name of "russian invaders".
While I would have loved to create a fully original game completely from scratch, time was running out and I still wished to finish covering the basics of Golang.
Because of this, I chose to dedicate the remainder of my time to finishing writing notes and example code in Golang into my Jupyter notebook `GO-2.pynb`.


## Additional Notes
- To run `spaceinvaders.py`, `russianinvaders.py`, or `wormy.py` you must have the plugin `pygame` installed on your device
- In order to run `spaceinvaders.py` or `russianinvaders.py`, you must download the appropriate `.zip` file and run the python code from there (otherwise it will not be able to find the files it needs to run (ie. sounds, images, & fonts))
- GitLab doesn't recognize Go as a language in the Jupyter notebooks, so you must download and open `GO-1.pynb` and `GO-2.pynb` onto your computer, and launch them from there (either download `lgo` or select `Continue Without Kernel when opening the file through Jupyter)


# ------------------------------------------------------------------------------


# Journal / Notebook


## FRIDAY:
For this project I am going to try and write an original piece of software.
The language used will most likely be Python, as it is the language that I am most comfortable with, and this is a rather large and difficult endeavour.
The software will most likely take the form of some kind of arcade-style game, as I've always wanted to create my own game.
    
I will start learning how to make actual software by consulting and trying to follow tutorials found on the internet.
First up, I will try to emulate the old "snake" game in Python3, as I have found a wonderful easy-to-follow tutorial about this online (https://inventwithpython.com/pygame/).
    

## Saturday:
So, it turns out that writing a program (like a game) in Python (or any language) is rather difficult, and a crazy amount of work.
I'm going to see if I can finish this snake tutorial, and then I may have to re-consider my final project idea.
Maybe run some kind of simpler code, or try learning the basics of another language (probably Google's Go language).
        
I have finished the "worms" tutorial (as in writing the code), but the program refuses to run on my computer.
I will do my best to trouble-shoot the code to get it working on my computer, before progressing on my project.
    
After extensive trouble-shooting, I found out that the code wasn't running due to my computer not having the `pygame` plugin installed.
After hours spent trying to install it on my Mac (is much harder than it sounds), I appear to have screwed my computer up.
Basic functions like: `cd`, `ls`, or `ssh` no longer work in terminal, and I cannot resolve the issue, no matter how many times I try to re-start my computer.
When I try to run something, I get an error message stating: `-bash: ls: command not found`, where the `ls` is replaced with whatever command I am attempting to pass to the shell.
I'm genuinely worried that I may have permanently screwed up something on my computer, and should probably check it with Ian on Tuesday.

I have uploaded my Python code, and will submit my Jupyter file once I have annotated it further.
Due to difficulties with Python now (computer doesn't recognize Python2 or Python3 as being installed), I'm probably going to change my project towards something along the lines of learning a new language.

Turns out that my weird terminal mishap also screwed up Python, as terminal no longer recognizes neither Python2 nor Python3.
I reinstalled the newest Python2 and Python3 online.
My terminal now launches when instructed to launch Python2 or Python3, but still craps out when I just tell it to launch Python, stating `-bash: /usr/local/Cellar/python3/3.4.2_1/Frameworks/Python.framework/Versions/3.4/bin/python3.4: No such file or directory`. 

It turns out that my computer was f*cked because my path variables had gotten all screwed up.
After extensive googling, I was able to repair my terminal with the command: `export PATH="/usr/local/bin:/usr/bin:/bin:/usr/sbin"`.
This lets me use basic functions like: `ls`, `cd`, and `ssh` freely in that terminal window again.


## Sunday
Turns out there are still some terminal issues present, meaning I have to write `PATH="/usr/local/bin:/usr/bin:/bin:/usr/sbin"` into every terminal window I open, otherwise the new window goes back to being broken.
I should probably ask Ian about the long-tern consequences of this, and see if I can get terminal working normally again.

It would also appear that I broke Jupyter when I messed my computer up yesterday.
Though thankfully, everything seems alright again after I re-installed Jupyter again using the `pip` package management system.

I also finished and uploaded the “worms" Jupyter file, though I will go back and add some more comments for myself later.
I also conducted some research into how to install `pygame` on the Rasberri Pi.
I might try to do this to the clasroom Pi, and then test run my "worms" code on it.

I then uploaded my annotated and completed "worms" Jupyter noteookm with all of my comments onto GitLab.

I tried downloading `pygame` onto the class Rasberry Pi, but got the prompt: `[sudo] password for quest:`.
After typing in the password "kermode1", I got an error message stating: `quest is not in the sudoers file.  This incident will be reported.`.
After googling around, I found that the newest version of Rasbian should already have `pygame` installed.
So, I moved my `worms.py` file over to the class network file system (nfs), and tried running it on the Rasberry Pi.
The loading screen booted up just fine, but the game doesn't seem to get to the point where it even tries to run.
I don't know if this is a problem with my code, or with the modules installed on the Rasberri Pi.
I'll ask Ian about this later.

GitLab was down in the evening, which meant that I lost all my previous changes to this README document (as I had it running and hadn’t saved it), and couldn't access or update anything here for a few hours.
In the meantime, I tried to familiarize myself with the basics of the Go Language, by writing a simple "Hello World" code.
Onve this code was finished, I tried to run it, and once again got an error message, this time stating: `-bash: go: command not found`.
This was annoying, so I went to re-download and re-install Golang.
However, this proved to be impossible, as the installer refused to re-install Go, stating that there was already a version of Golang installed.
Great... here comes my broken-ass terminal path to screw me over again, but this time export `PATH="/usr/local/bin:/usr/bin:/bin:/usr/sbin"` doesn't seem to help.
So, I took to stack overflow and the Golang forums, and was eventually able to find a line of code that might help: `export PATH=$PATH:/usr/local/go/bin`.
This managed to fix up yet another problem in my terminal, so that I was able to run Golang, though this solution was once again limited to the specific windows where the aforementioned code had been inserted.
I really have to get this checked out.
Anyways, I test-ran my "Hello World" code and found that it ran absolutely fine, so I commented the sh*t out of it and submitted it to my GitLab page.
I will spend more time tomorrow learning about Golang, using several in depth video tutorials and lessons that I found on the internet today.


## Monday
Yesterday before going to bed, I made an account and posted a question on stack overflow, begging for help with my computer (https://stackoverflow.com/questions/51129115/terminal-bash-command-not-found-errors/51129164?noredirect=1#comment89243907_51129164).
I stayed up too late, writing back and forth with VonC, but all I was able to get out of it was that there is a problem in my path... which is the only thing I ALREADY KNEW!!! Argh.

This morning, I tried tons of different ways to re-set/fix my path (mostly from stack overflow and stack exchange), but every solution only fixes my terminal path for that one window.
Every new window I add is back to being broken for some reason.
This is really frustrating, and I'll have to ask Ian for some help on Tuesday.

Having given up on saving my dying terminal, I proceeded to start learning Golang.
One of the first things I decided would be beneficial to me, would be to make a new Jupyter notebook, so that I had a place to neatly write down notes about this new language I was learning.
I knew that Jupyter could support multiple languages, but I was only getting an option for Python3 when I tried to create a new notebook.
I proceeded onto the internet, and found that there are two ways to get Golang as an option alongside the default Python3 when creating a new notebook in Jupyter.
The first one is called `gophernotes`, and can be found on https://github.com/gopherdata/gophernotes.
However, it was impossible to implement/download on my Mac, so I continued searching.
Eventually I stumbled upon another alternative by the name of  `lgo` on https://medium.com/@yunabe/interactive-go-programming-with-jupyter-93fbf089aff1.
This alternative seemed much better than the previous one, but was unfortunately only availble for Linux, meaning it wasn't compatible with my computer without the use of a virtual machine.
There was some good news though, as the author Yu Watanabe had published some example notebooks on that link as well.
This meant that I was able to download one of his notebooks as a `.ipynb` file, and then open it in my own Jupyter.
I obviously got an error message stating `Could not find a kernel matching Go (lgo). Please select a kernel:` and asking me to chose Python3 out of a dropdown menue with only that as an option, but if I clicked the giant red button labled `Continue Without Kernel`, the notebook opened without any issues.
Not only did it open, but the Golang enviroment Yu Watanabe developed was there and fully functional!
Now this means that while I can NOT create a new Golang notebook in Jupyter, I CAN create as many duplicates of his example notebook as I wish.
As long as I continue to use these as a template and delete all of his text before writing my own, I can successfully use `lgo` on Jupyter without any problems (at least as of yet).
I double checked the functionality by incorporating my `hello.go` into my new `lgo` notebook, and was pleasantly suprised to find that everything worked flawlessly (for once).
Who could have known?

So, after working on my notebook for a bit, I've found out that there is one slight "problem" to my Golang Jupyter notebook.
It isn't particularily serious, but the way I'm running it doesn't let me `run` my code directly in my notebook.
This means that I have to either copy-and-paste everything into my terminal, or into a text document that I will lable with a `.go` extention, and then run via terminal.

I looked through the user manual for Go, and copied down notes on things like basic syntax into my `GO.ipynb` notebook.
I tried to get the formatting all pretty, and uploaded what I have so far onto GitLab.
The formatting looks a bit off on GitLab as GitLab thinks the code is in Python3 even though it is in actuality Go Language.
There isn't really anything I can do about that, but it shouldn't really matter as Jupyter understands it as Go if you were to actually open the file with it.

I continued working my way through a fabulous YouTube tutorial that did a really good job explaing the basics of Golang, and how to use it effectively and efficiently (https://www.youtube.com/watch?v=C8LgvuEBraI)
I'm working on taking extensive notes, as I want to dedicate more time to getting better at Golang during the summer.


## Tuesday
Ian managed to fix my terminal in class this morning.
It turns out that the `.bash_profile` on my computer had been overwritten and screwed up by some bad code, which wasn't even Mac compatible.
I also introduced Aria to Golang, and got her started with a brief tutorial and instructions on how to create a "Hello World" file.
Chase decided he would also make a game, and together we were able to find a way to get `pygame` running on our computers (running out programs through PyCharm instead of either terminal or IDLE).

With `pygame` sort-of working on my computer, I really want to go back towards making an original piece of software.
However, I also wish to get better at Golang, and maybe re-code some of out Meeus examples from ealier on in the course using Go.

With an impending due date of tommorow, I opted to focus on modifying some pre-exising Python code using `pygame`.
I found and downloaded a copy of the classic arcade game *space invaders* in Python on GitHub (https://github.com/leerob/Space_Invaders), which can be found on the main page under the name `spaceinvaders.py`.
If anybody wishes to run this code, you must download the `spaceinvaders.zip` and run the code from there (it needs to be in the same location as the files it calls on).

I then started to edit this code as well as the files it calls on, to create a more unique gaming experience.
I started by changing the images of the enemy spacecraft to pictures of: the russian hammer & sickle, russian stacking dolls, and floating images of Putin's head.
I then proceded to make the mystery spacecraft an image of a shirtless Putin riding a bald eagle.
Once I had successefully changed the invading forces from space aliens to russians, I needed a new name.
I settled on **Russian Invaders**, as a play on the original space invaders concept.
I then had to go into my code and change the loading screen to display the updated title and image classes.
Now that we had our invading forces covered, I needed to work on the playable character.
I ended up settling on using a fat child riding an american eagle, with a burger in one hand and a machine gun in the other.
After some quick photoshopping and the inevitable trouble-shooting, I was able to get the code running pretty smoothly.
However, the fact that the Russians and the Americans were shooting lazers still bothered me, so I went in and changed the lazer shots into miniature Solviet Unian and American flags.
I also went in and changed the sounds effects of the shooting from lazer shots, to Russian men saying водка (vodka) and the screech of an American eagle.
However, these sound effect additions became incredibly annoying after a few seconds, so I re-set the audio back to the original lazer sounds.
Overall, I'm very pleased with how my code turned out, and can't wait to try actually create my own minigame from scratch sometime in the coming months.

With `pygame` semi-working and the sweet taste of victory from a successeful minigame in my mouth, I went back to my `wormy` code.
It turns out that there wasn't any problem in the class Rasberry Pi, and that the game was not getting past the initial start screen due to a mistake in my code.
With the ability to now run my code, I was able to isolate said error, and get `wormy` to run without any errors on my device.
Yaaaaaaaaaaaaay!

Even though it is starting to get a bit late, I still wanted to finish writing my Go notes from the YouTube video I had started watching.
So, I sat my a$$ down and got through another 3 sections.
I think this only leaves one or two more sections to do tommorow before I have to hand it in, which should be more than doable.

If I end up having some extra time, I'd like to do re-program some of the Meeus examples from earlier on in this course using Go.
I've also come up with a concept for an original-ish game that shouldn't be too hard to code (it's a castle defence game style of game where you're a llama that spits at the attacking forces to keep them at bay).


## Wednesday
Today, I managed to finish my `GO-2.pynb` notebook on the Go Language.

Overall this has been a great journey, and I can't wait to keep building from this knowledge in the future!


